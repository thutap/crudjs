var dssv =[];
var base_url ="https://64f82cc9824680fd217f3782.mockapi.io/showshop";
//loading
var batloading = function()
{
    document.getElementById("loading").style.display="flex";
};
var tatloading = function()
{
    document.getElementById("loading").style.display="none";
};
/// thong bao

const themMessage = document.getElementById("themmessage");


function showthemmessage() {
    themMessage.style.display = "block";

  setTimeout(() => {
    themMessage.style.display = "none";
  }, 3000); 
}
//
const capnhapMessage = document.getElementById("capnhapmessage");


function showcapnhapMessage() {
    capnhapMessage.style.display = "block";

  setTimeout(() => {
    capnhapMessage.style.display = "none";
  }, 3000); 
}
//
const xoaMessage = document.getElementById("xoamessage");


function showxoaMessage() {
    xoaMessage.style.display = "block";

  setTimeout(() => {
    xoaMessage.style.display = "none";
  }, 3000); 
}



///
var renderTable = function(list)
{
    var contentHTML=" ";
    list.forEach(function(item) 
    {
        var trContent =   `
        <tr>
            <td>${item.id}</td>
            <td>${item.name}</td>
            <td>${item.company}</td>
            <td>
            <img src = ${item.img} style="width:90px" alt=""/>
            </td>
            <td>${item.price}$</td>
            <td>
            <button onclick="xoasanpham('${item.id}')" class ="btn btn-danger"> Xóa </button>
            </td>
            <td>
            <button onclick="suasanpham('${item.id}')" class ="btn btn-danger"> Sửa </button>
            </td>

        </tr>
        `;
        contentHTML += trContent;
        
    });
    document.getElementById("tbodySanpham").innerHTML = contentHTML;
};
var renderdssvservice = function()
{
    axios({
        url:`${base_url}/shoe`,
        method:"GET",

    })
    .then(function(res)
    {
        dssp = res.data;
        renderTable(dssp);

    })
    .catch(function(err)
    {
        console.log(err);
    });
}
renderdssvservice();
function xoasanpham(id)
{
    batloading();
    console.log("id: ",id);
    axios({
        url:`${base_url}/shoe/${id}`,
        method:"DELETE",
    })
    .then(function(res)
    {
        tatloading();
        renderdssvservice();
        renderTable(dssp)
        showxoaMessage();
    })
    .catch(function(err)
    {
        tatloading();
        console.log(err);
    });
}
// sua san pham
 function suasanpham(id){
    console.log("id",id);

    batloading();
    axios ({
        url:`${base_url}/shoe/${id}`,
        method:"GET",

    })
    .then(function(res)
    {
      
        tatloading();
        console.log(res);
        showthongtin(res.data);
    })
    .catch(function(err)
    {
        tatloading();
        console.log(err);
    })
 }

 // them san pham

 function themSP(id){
    console.log("id",id);
    var dataFrom = laythongtintuform();
    batloading();
    axios({
        url:`${base_url}/shoe`,
        method:"POST",
       data: dataFrom,
    })
    .then(function(res)
    {
        tatloading();
        renderdssvservice();
        console.log(res);
        showthemmessage();
    })
    .catch(function(err){
        tatloading();
        console.log(err);
    })

 }
 //cập nhập
 function capNhatSP(){
    var dataFrom = laythongtintuform();
    console.log("dataFrom: " ,dataFrom);
    batloading();
    axios({
        url:`${base_url}/shoe/${dataFrom.id}`,
        method:"PUT",
        data:dataFrom,
    })
    .then(function(res){
        tatloading();
        renderdssvservice();
        console.log(res);
        showcapnhapMessage()

    })
    .catch(function(err){
        tatloading();
        console.log(err);
    })
 }